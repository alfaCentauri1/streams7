package com.alfaCentauri;

import java.util.List;

public class Ejecutable {

    public static void main(String args[]) {
        System.out.println("Operaciones con Streams y el metodo de ordenamiento.");
        List<String> books = List.of("El Señor de los Anillos", "Don Quijote de la Mancha", "Historia de dos ciudades",
                "El Hobbit", "El Principito");
        var stringWithSeparator = String.join(";", books);
        //Imprimiendo con un stream
        books.stream().forEach( book -> System.out.println( "* " + book ) );
        //Imprimiendo despues de unir
        System.out.println( "Despues de unir las cadenas de caracteres:" );
        System.out.println( stringWithSeparator );
    }

}
